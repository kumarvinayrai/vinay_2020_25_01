setInterval(() => {
    makeRequest();
}, 1000);
makeRequest();
var httpRequest;
let NumberCanvas = 0;
let selectedValue = '';
let insertCount = 0;
var canvases = [];
var targetCanvas;
var pendingG;
var activeGrouppp;
var migrateItem = function(fromCanvas, toCanvas, pendingImage) {
   
    var pendingTransform = fromCanvas._currentTransform;
    fromCanvas._currentTransform = null;
	
	var removeListener = fabric.util.removeListener;
    var addListener = fabric.util.addListener; {
        removeListener(fabric.document, 'mouseup', fromCanvas._onMouseUp);
        removeListener(fabric.document, 'mousemove', fromCanvas._onMouseMove);
        addListener(fromCanvas.upperCanvasEl, 'mousemove', fromCanvas._onMouseMove);
    } {
        removeListener(toCanvas.upperCanvasEl, 'mousemove', toCanvas._onMouseMove);
        addListener(fabric.document, 'mouseup', toCanvas._onMouseUp);
        addListener(fabric.document, 'mousemove', toCanvas._onMouseMove);
    }
	
	if(pendingImage._objects){
		activeGrouppp=fromCanvas.getActiveObject();
		//add code
		pendingG=Object.assign({},pendingImage);
		
				for(i=0;i<pendingImage._objects.length;i++){
					 fromCanvas.remove(pendingImage._objects[i]);
						timeOut(i,toCanvas,pendingTransform);
					}
			}
			else{
				  fromCanvas.remove(pendingImage);
				  setTimeout(function() {
        pendingImage.canvas = toCanvas;
        pendingImage.migrated = true;
        toCanvas.add(pendingImage);
        toCanvas._currentTransform = pendingTransform;
        toCanvas.setActiveObject(pendingImage);
    }, 10);
				 }
    
};
function timeOut(i,toCanvas,pendingTransform){
				 setTimeout(function() {
						pendingG._objects[i].canvas = toCanvas;
						pendingG._objects[i].migrated = true;
						toCanvas.add(pendingG._objects[i]);

						toCanvas._currentTransform = pendingTransform;
						toCanvas.setActiveObject(activeGrouppp);
					
				}, 10);
}
var clearCanvas = function(p) {
    targetCanvas = undefined;
}

var getTargetCanvas = function(p) {
    if (p.e.currentTarget.previousElementSibling && p.e.currentTarget.previousElementSibling.id !== undefined) {
        var idx = p.e.currentTarget.previousElementSibling.id;
        var o = idx.substring(1);
        targetCanvas = canvases[o];
    }
}

var onObjectMoving = function(p) {
    if (targetCanvas !== undefined) {
        if (p.target.canvas.lowerCanvasEl.id != targetCanvas.lowerCanvasEl.id) {
		
				 console.log( p.target.canvas.lowerCanvasEl.id +' '+ targetCanvas.lowerCanvasEl.id);
				 migrateItem(p.target.canvas, targetCanvas, p.target);
			
            return;
        }
    }
};

function getRandomCanvas() {
    let NumberArray = [2, 3, 4, 5];
    NumberCanvas = NumberArray[Math.floor(Math.random() * NumberArray.length)];
    let element = document.getElementById('canvasIds');
    let canvasHtml = '';
    for (let index = 0; index < NumberCanvas; index++) {
        canvasHtml += '<canvas id="C' + index + '"></canvas>';
    }
    element.innerHTML = canvasHtml;
    for (let index = 0; index < NumberCanvas; index++) {
        canvases[index] = new fabric.Canvas('C' + index);
        canvases[index].renderAll();

        canvases[index].on("object:moving", onObjectMoving);
        canvases[index].on("mouse:move", getTargetCanvas);
        canvases[index].on("mouse:up", clearCanvas);
    }
    insertCount = 0;
}

function makeRequest() {
    httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = alertContents;
    httpRequest.open('GET', 'https://jsonplaceholder.typicode.com/photos');
    httpRequest.send();
}

function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            let data = JSON.parse(httpRequest.responseText);
            let selectHtml = '';
            let selectElement = document.getElementById('select-dropdown');
            data = shuffle(data);
            for (let index = 0; index < 10; index++) {
                selectHtml += '<option value=' + data[index].thumbnailUrl + '>' + data[index].title + '</option>';
            }
            selectElement.innerHTML = '<select class="center-content" onChange="addImageToCanvas(event)" id="title" style="margin: 0 20px;padding: 6px;border-radius: 5px;border: 1px solid;">' + selectHtml + '</select>';
            selectedValue = data[0].thumbnailUrl;
        } else {
            alert('There was a problem with the request.');
        }
    }
}

function addImageToCanvas(event) {
    selectedValue = event.target.value;
}

function insertImageToCanvas() {
    if (insertCount <= NumberCanvas) {
        fabric.Image.fromURL(selectedValue, function(myImg) {
            var img1 = myImg.set({ left: 0, top: 0 });
            if (canvases.length > insertCount) {
                canvases[insertCount].add(myImg);
                insertCount++;
            }
        });
    }
}

function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}